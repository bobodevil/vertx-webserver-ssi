# SSI include application

A proxy pass like request retrieving html from a mock cms server, Based on a given string pattern of the response, a separate request is executed (Api dynamic replacement) to replace the string. This replacements occurs until no known patterns are left and finally streaming the output of the page back to the response object when complete.

cd to root directory

#Prepare
Edit <Path to the repository>/container-module/src/main/resources/config.json
Update "handlebar-root", and "web_root" to relvent directories. Example:

"web_root" : "/place/i/checked/out/the/code/vertx-webserver-ssi/web-module/web"

and 

"handlebar-root" : "/place/i/checked/out/the/code/vertx-webserver-ssi/web-module/template/handlebar"


#Build (Test has currently been skipped due to many dummy test case)
mvn clean install -Dmaven.test.skip=true

#Execute
vertx runmod com.bobodevil~container-module~0.1 -conf "<Path to the repository>/container-module/src/main/resources/config.json"