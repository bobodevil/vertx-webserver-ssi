package com.bobodevil.html;


import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class HTMLSplitter {
 
	private Pattern patternTag, patternLink;
	private Matcher matcherTag, matcherLink;
 
	private static final String HTML_A_TAG_PATTERN = "(?i)<div data-module=\"module\" ([^>]+)>(.+?)</div>";
	
	private static final String HTML_A_HREF_TAG_PATTERN = 
			"\\s*(?i)data-module-type\\s*=\\s*\"(.*?)\"" +
			"\\s*(?i)data-module-template\\s*=\\s*\"(.*?)\"" +
			"\\s*(?i)data-module-data\\s*=\\s*\"(.*?)\"";
 
 
	public HTMLSplitter() {
		patternTag = Pattern.compile(HTML_A_TAG_PATTERN);
		patternLink = Pattern.compile(HTML_A_HREF_TAG_PATTERN);
	}
	
	public HtmlSplit[] getHtmlSplit(final String html) {
		Vector<HtmlSplit> result = new Vector<HtmlSplit>();
		
		Vector<HtmlSplit> split = grabHTMLLinks(html);
		if(split == null || split.size() == 0 ) {
			HtmlSplit nonHtml = new HtmlSplit();
			nonHtml.setHtml(html);
			result.add(nonHtml);
		}
		else {
			int last = 0;
			HtmlSplit lastSplit = null;
			for (HtmlSplit v : grabHTMLLinks(html)) {
				if (v.getStart() > last) {
					HtmlSplit htmlObj = new HtmlSplit();
					htmlObj.setHtml(html.substring(last,v.getStart()));
					result.add(htmlObj);
					
				}
				result.add(v);
				last = v.getEnd();
			}
			HtmlSplit htmlObj = new HtmlSplit();
			htmlObj.setHtml(html.substring(result.get(result.size()-1).getEnd(),html.length()));
			result.add(htmlObj);
			
		}
		return result.toArray(new HtmlSplit[result.size()]);
	}
 
	/**
	 * Validate html with regular expression
	 * 
	 * @param html
	 *            html content for validation
	 * @return Vector links and link text
	 */
	public Vector<HtmlSplit> grabHTMLLinks(final String html) {
 
		Vector<HtmlSplit> result = new Vector<HtmlSplit>();
 
		matcherTag = patternTag.matcher(html);
 
		while (matcherTag.find()) {
			
			
			
			HtmlSplit obj = new HtmlSplit();
			
			obj.setHtml(html.substring(matcherTag.start(), matcherTag.end()));
			obj.setStart(matcherTag.start());
			
			obj.setEnd(matcherTag.end());
			
			String attribute = matcherTag.group(1); // href
			
			String moduleContent = matcherTag.group(2); // link text
			
			matcherLink = patternLink.matcher(attribute);
			
			obj.setModuleContent(moduleContent);
			while (matcherLink.find()) {
 
				String type = matcherLink.group(1);// module type
				String template = matcherLink.group(2);// template
				String data = matcherLink.group(3);// template
				//System.out.println("type " + type);
				//System.out.println("data " + data);
				//System.out.println("template " + template);
				//System.out.println("====");
				
				obj.setModuleType(type);
				obj.setModuleData(data);
				obj.setModuleTemplate(template);	
 
				result.add(obj);
 
			}
 
		}
 
		return result;
 
	}
 
	public class HtmlSplit {
 
		String html;
		String moduleType;
		String moduleData;
		String moduleTemplate;
		String moduleContent;
 
		Integer start;
		Integer end;
		
		HtmlSplit(){}

		public String getHtml() {
			return html;
		}

		public void setHtml(String html) {
			this.html = html;
		}

		public String getModuleType() {
			return moduleType;
		}

		public void setModuleType(String moduleType) {
			this.moduleType = moduleType;
		}

		public String getModuleContent() {
			return moduleContent;
		}

		public void setModuleContent(String moduleContent) {
			this.moduleContent = moduleContent;
		}

		public Integer getStart() {
			return start;
		}

		public void setStart(Integer start) {
			this.start = start;
		}

		public Integer getEnd() {
			return end;
		}

		public void setEnd(Integer end) {
			this.end = end;
		}

		public String getModuleData() {
			return moduleData;
		}

		public void setModuleData(String moduleData) {
			this.moduleData = moduleData;
		}

		public String getModuleTemplate() {
			return moduleTemplate;
		}

		public void setModuleTemplate(String moduleTemplate) {
			this.moduleTemplate = moduleTemplate;
		};
		
	}
}