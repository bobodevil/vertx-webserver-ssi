package com.bobodevil.ssi;


import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.platform.Verticle;


public class ServerSideIncludeVerticle extends Verticle {
	public void start() {
		ServerSideIncludeHandler ssiHandler = new ServerSideIncludeHandler(vertx, container);
		
		EventBus eb = vertx.eventBus();
		eb.registerHandler("com.bobodevil.ssi.process", ssiHandler.processInclusion());
	}
}
