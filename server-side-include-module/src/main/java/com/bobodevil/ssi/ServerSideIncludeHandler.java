package com.bobodevil.ssi;

import java.util.concurrent.atomic.AtomicInteger;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import com.bobodevil.html.HTMLSplitter;
import com.bobodevil.html.HTMLSplitter.HtmlSplit;

public class ServerSideIncludeHandler {
	
	private Vertx vertx;
	private Container container;
	
	
	public ServerSideIncludeHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
	}
	
	public Handler<Message<JsonObject>> processInclusion() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				getInclusionOutput(message.body().getString("data"), message);
			}
		};
	}
	
	private void getInclusionOutput(String beforeProcess,final Message<JsonObject> message) {
		final JsonObject returnObject = new JsonObject();
    	
        
		HTMLSplitter splitter = new HTMLSplitter();
		final HtmlSplit[] split = splitter.getHtmlSplit(beforeProcess);
		
		final AtomicInteger count = new AtomicInteger(0);
		for (HtmlSplit html : split) {
			if (html.getModuleType() != null) {
				count.incrementAndGet();
			}
		}
		
		final AtomicInteger moduleCallCount = new AtomicInteger(0);
		
		if (split.length > 1) {
			for (HtmlSplit html : split) {
				
				if (html.getModuleType() != null) {
					
					JsonObject jsonObject = new JsonObject();
					jsonObject.putString("moduleTemplate", html.getModuleTemplate());
					jsonObject.putString("moduleData", html.getModuleData());
					jsonObject.putString("sessionId", message.body().getString("sessionId" , ""));
					
					final HtmlSplit s = html;
					final String moduleEvent = "com.bobodevil.module."+ html.getModuleType() + ".display";
					final Integer eventTimeout = 200; //100ms;
					vertx.eventBus().sendWithTimeout(
							 moduleEvent, jsonObject, eventTimeout ,new Handler<AsyncResult<Message<JsonObject>>>() {
						@Override
						public void handle(AsyncResult<Message<JsonObject>> asyncMessage) {
							if (asyncMessage.succeeded()) {
								JsonObject obj = asyncMessage.result().body();
								
								String output = obj.getString("data");
								
								vertx.eventBus().send("com.bobodevil.logger.debug", 
										new JsonObject().putString("data", "replacing data : " + output));
								
								s.setHtml(output);
								if (moduleCallCount.incrementAndGet() == count.intValue()) {
									returnObject.putString("status", "ok");
									returnObject.putString("data", buildOutput(split));
									
	    							message.reply(returnObject);
	
								}
							} else {
								if (moduleCallCount.incrementAndGet() == count.intValue()) {
									vertx.eventBus().send("com.bobodevil.logger.info", 
											new JsonObject().putString("data", "Even Timeout " + 
														eventTimeout + ", " + moduleEvent));
									returnObject.putString("data", buildOutput(split));
									message.reply(returnObject);
								}
							}
						}
						
					});
					
				}
			}
		} else {
			returnObject.putString("data", buildOutput(split));
			message.reply(returnObject);
		}
	}
	
	private String buildOutput(HtmlSplit[] split) {
		StringBuffer sb = new StringBuffer("");
		for (HtmlSplit h : split) {
			sb.append(h.getHtml());
		}
		return sb.toString();
	}
	
}
