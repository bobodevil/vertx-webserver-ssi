package com.bobodevil.unit;

import java.util.Vector;

import junit.framework.Assert;

import com.bobodevil.html.HTMLSplitter;
import com.bobodevil.html.HTMLSplitter.HtmlSplit;
 
/**
 * HTML link extrator Testing
 * 
 * 
 */
public class TestHTMLSplitter {
 
	private HTMLSplitter htmlLinkExtractor;
 
	@org.junit.Test
	public void ValidHTMLLinkTest() {
		htmlLinkExtractor = new HTMLSplitter();
		String html = "asdasdas <div data-module=\"module\" data-module-type=\"shoppingcart\" data-module-template=\"shoppingcart.hbs\" data-module-data=\"{'test':'test'}\">This is cool</div><div data-module=\"module\" data-module-type=\"shoppingcart\" data-module-template=\"shoppingcart.hbs\" data-module-data=\"{'test':'WOWt'}\">This is cool</div> "
				+ "asdasdasdasdads <div data-module=\"module\" data-module-type=\"shoppingcart\" data-module-template=\"shoppingcart.hbs\" data-module-data=\"{'test':'test'}\">This is cool</div> asdassd";
		HtmlSplit[] splitter = htmlLinkExtractor.getHtmlSplit(html);
		
		for (HtmlSplit s: splitter) {
			System.out.println(s.getHtml());
		}
		Assert.assertEquals(6, splitter.length);
	}
}