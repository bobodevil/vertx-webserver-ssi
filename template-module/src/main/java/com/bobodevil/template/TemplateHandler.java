package com.bobodevil.template;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.CaseInsensitiveMultiMap;
import org.vertx.java.core.http.HttpClient;
import org.vertx.java.core.http.HttpClientRequest;
import org.vertx.java.core.http.HttpClientResponse;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;
import org.vertx.java.platform.Verticle;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.FileTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;


public class TemplateHandler {
	
	private Map<String, Object> templateMap;
	private Vertx vertx;
	private Container container;
	
	public TemplateHandler(Map<String, Object> templateMap) {
		this.templateMap = templateMap;
	}
	
	public TemplateHandler(Vertx vertx, Container container, Map<String, Object> templateMap) {
		this.templateMap = templateMap;
		this.container = container;
		this.vertx = vertx;
	}
	
	public Handler<Message<JsonObject>> templateHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				String incomingTemplate = message.body().getString("template");
				String incomingData = message.body().getString("data");
				
				
				
				//vertx.eventBus().send("com.bobodevil.logger.info", 
				//		new JsonObject().putString("data", "template : " + incomingTemplate));
				
				String returnData = null;
				try {
					returnData = processTemplate(incomingTemplate, incomingData);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				JsonObject responseObject = new JsonObject();
				
				responseObject.putString("status", "ok");
				
				responseObject.putString("data", returnData);
				
				//vertx.eventBus().send("com.bobodevil.logger.info", 
				//		new JsonObject().putString("data", "data : " + returnData));
				
				message.reply(responseObject);
			}
		};
	}
	
	public String processTemplate(String templateLocation, String data) throws IOException {
		String output = null;
		if (templateLocation.endsWith(".hbs")) {
			String templateRoot = (String)templateMap.get("handlebar-root");
			
			if ("".equals(templateRoot) || templateRoot == null) {
				templateRoot = this.getClass().getClassLoader().getResource("template/handlebar").getPath();
			}
			
			output = processHandleBarTemplate(templateRoot, templateLocation, data);
			
		}
		return output;
	}
	
	private String processHandleBarTemplate(String templateRoot, 
						String templateLocation, String data) throws IOException {
		String filePath = templateRoot + templateLocation;
		
		File templateFile = new File(filePath);
		TemplateLoader loader = new FileTemplateLoader(templateFile.getParentFile());
		Handlebars handlebars = new Handlebars(loader);
		Template template = handlebars.compile(FilenameUtils.removeExtension(templateFile.getName()));
		return template.apply(data);
	}
}
