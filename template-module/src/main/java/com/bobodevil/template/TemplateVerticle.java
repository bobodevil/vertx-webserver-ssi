package com.bobodevil.template;


import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;



public class TemplateVerticle extends Verticle {

	public void start() {
		JsonObject appConfig = container.config();
		
		EventBus eb = vertx.eventBus();
		
		JsonObject templates = appConfig.getObject("template-location");
		
		TemplateHandler templateHandler = new TemplateHandler(vertx, container ,templates.toMap());
		
		eb.registerHandler("com.bobodevil.template", templateHandler.templateHandler());
		
		container.logger().info("Template Handler Started");
	}
}
