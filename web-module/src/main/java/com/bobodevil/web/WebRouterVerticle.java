package com.bobodevil.web;

import java.io.File;

import org.vertx.java.busmods.BusModBase;
import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.core.json.JsonObject;

import com.bobodevil.web.handler.RouteHandler;

public class WebRouterVerticle extends BusModBase {
	
	
	public void start() {
		super.start();
		
		startServer();
		startServerMockCMS();
		//startServerMockAPI();
		container.logger().info("WebRouterVerticle started");
	  }
	
	public void startServerMockCMS(){
		final JsonObject appConfig = container.config();
		
		vertx.createHttpServer().requestHandler( new Handler<HttpServerRequest>() {
			@Override
			public void handle(HttpServerRequest req) {
				String webRoot = appConfig.getString("web_root","");
						
				if ("".equals(webRoot) || webRoot == null) {
					webRoot = this.getClass().getClassLoader().getResource("web").getPath();
					if (webRoot.indexOf(":") != -1){
						webRoot = webRoot.substring(webRoot.indexOf(":") - 1);
					}
				}
				String webRootCMS = webRoot + "/mockcms";
				String fileName = req.uri();
				String webRootPrefix = webRootCMS;
				final String fileResponse = webRootPrefix + fileName;
				container.logger().info("WOW" + fileResponse);
				
				req.response().sendFile(fileResponse);
				
			}
		}
		).listen(8282);
	}
	/**
	public void startServerMockAPI(){
		JsonObject appConfig = container.config();
		
		String webRoot = appConfig.getString("web_root") + File.separator+"mockapi";
		String index = "index.html";
		String webRootPrefix = webRoot + File.separator;
		final String indexPage = webRootPrefix + index;
		
		
		vertx.createHttpServer().requestHandler( new Handler<HttpServerRequest>() {
			
			@Override
			public void handle(HttpServerRequest req) {
				req.response().sendFile(indexPage);
				
			}
		}
		).listen(8283);
	}
	**/
	private void startServer() {
		HttpServer server = vertx.createHttpServer();
		
		RouteHandler rh = new RouteHandler(vertx, container);
		rh.configRoutes(server);
		server.listen(8889);
	}
	
	
}