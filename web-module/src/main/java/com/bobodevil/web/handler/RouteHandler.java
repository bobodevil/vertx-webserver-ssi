package com.bobodevil.web.handler;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;

import java.io.File;
import java.util.Map;

import org.vertx.java.core.Handler;
import org.vertx.java.core.MultiMap;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import rx.Observable;
import rx.util.functions.Action1;

import com.bobodevil.utils.ConvertMapFromMultiMap;
import com.bobodevil.web.utils.RequestUtils;


public class RouteHandler {
	
	private Vertx vertx;
	private Container container;
	
	
	public RouteHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
	}
	
	public void configRoutes(HttpServer server) {
		final JsonObject appConfig = container.config();
		
		final EventBus eb = vertx.eventBus();
		
		RouteMatcher matcher = new RouteMatcher();
		
		matcher.all("/module/:modulename/:displaytype", new Handler<HttpServerRequest>() {
			@Override
			public void handle(final HttpServerRequest req) {
				
				String modulename = req.params().get("modulename");
		        String displaytype = req.params().get("displaytype");
		        
				RequestUtils requestUtils = new RequestUtils();
				JsonObject jsonObject = requestUtils.getJsonObjectFromRequest(req);
				String moduleEvent = "com.bobodevil.module."+modulename+".dispatchhandler";
				ModuleWebHandler moduleWebHandler = new ModuleWebHandler(req);
				
				if ("service".equals(displaytype)) {
					eb.send(moduleEvent, jsonObject, moduleWebHandler.getServiceDisplay());
				} else {
					req.response().end("{\"status\":\"error\"}");
				}
			}
		});
		
		
		
		matcher.allWithRegEx("\\/static\\/([^\\\\]+)", new Handler<HttpServerRequest>() {
			@Override
			public void handle(HttpServerRequest req) {
				
				String webRoot = appConfig.getString("web_root","");
				
				if ("".equals(webRoot) || webRoot == null) {
					webRoot = this.getClass().getClassLoader().getResource("web").getPath();
					if (webRoot.indexOf(":") != -1){
						webRoot = webRoot.substring(webRoot.indexOf(":") - 1);
					}
				}
				String webRootCMS = webRoot + "/static";
				String fileName = req.uri();
				String webRootPrefix = webRootCMS;
				final String fileResponse = webRootPrefix + "/" + req.params().get("param0");
				container.logger().info("WOW" + fileResponse);
				
				req.response().sendFile(fileResponse);
				/**
				String webRoot = appConfig.getString("web_root") + File.separator+"static";
				String webRootPrefix = webRoot + File.separator + req.params().get("param0");
				final String filePath = webRootPrefix;
				
				//req.response().end(indexPage);
				req.response().sendFile(filePath);**/
			}
		});
		
		matcher.noMatch( new Handler<HttpServerRequest>() {
			@Override
			public void handle(final HttpServerRequest req) {
				
				RequestUtils requestUtils = new RequestUtils();
				final JsonObject jsonObject = requestUtils.getJsonObjectFromRequest(req);
				
				Observable<RxMessage<JsonObject>> obs = (new RxEventBus(eb)).send("com.bobodevil.session.id", 
						new JsonObject().putString("sessionId", jsonObject.getString("sessionId","")));
				
				obs.subscribe(new Action1<RxMessage<JsonObject>>() {
					public void call(final RxMessage<JsonObject> rxMessage) {
						jsonObject.putString("sessionId", rxMessage.body().getString("sessionId"));
						req.response().headers().add("Set-Cookie", "sessionId="+jsonObject.getString("sessionId"));
						
						eb.send("com.bobodevil.cms.request", jsonObject, new Handler<Message<JsonObject>>() {
							@Override
							public void handle(Message<JsonObject> message) {
								
								String data = message.body().getString("data");
								jsonObject.putValue("data", data);
								eb.send("com.bobodevil.ssi.process", jsonObject, new Handler<Message<JsonObject>>() {
									@Override
									public void handle(Message<JsonObject> message) {
										req.response().end(message.body().getString("data"));
									}
								});
							}
						
						});
					}
				});
			}
		});
		
		
		server.requestHandler(matcher);
	}
	
	
}
