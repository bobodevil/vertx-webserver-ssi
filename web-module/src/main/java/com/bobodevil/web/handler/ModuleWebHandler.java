package com.bobodevil.web.handler;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonObject;

public class ModuleWebHandler {
	private HttpServerRequest req;
	
	public ModuleWebHandler(HttpServerRequest req) {
		this.req = req;
	}
	
	public Handler<Message<JsonObject>> getServiceDisplay() {
		return new Handler<Message<JsonObject>>() {
			@Override
			public void handle(Message<JsonObject> message) {
				req.response().end(message.body().toString());
			}
		};
	}
}
