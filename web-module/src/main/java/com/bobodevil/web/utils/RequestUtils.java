package com.bobodevil.web.utils;

import io.netty.handler.codec.http.Cookie;
import io.netty.handler.codec.http.CookieDecoder;

import java.util.Set;

import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonObject;

import com.bobodevil.utils.ConvertMapFromMultiMap;

public class RequestUtils {
	public JsonObject getJsonObjectFromRequest(HttpServerRequest req) {
		JsonObject jsonObject = new JsonObject();
		
		ConvertMapFromMultiMap convertor = new ConvertMapFromMultiMap();
		jsonObject.putObject("params", new JsonObject(convertor.convert(req.params())));
		jsonObject.putString("method", req.method());
		jsonObject.putString("uri",req.uri());
		jsonObject.putObject("headers", new JsonObject(convertor.convert(req.headers())));
		
		String sessionId = "";
		
		String value = req.headers().get("Cookie");
		if (value != null) {
			Set<Cookie> cookies = CookieDecoder.decode(value);
			for (final Cookie cookie : cookies) {
				if ("sessionId".equals(cookie.getName())) {
					sessionId = cookie.getValue();
					
					jsonObject.putString("sessionId", sessionId);
					
				}
			}
		}
		
		
		
		return jsonObject;
	}
}
