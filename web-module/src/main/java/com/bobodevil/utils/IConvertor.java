package com.bobodevil.utils;


public interface IConvertor<To,From> {
	public To convert(From from);
}
