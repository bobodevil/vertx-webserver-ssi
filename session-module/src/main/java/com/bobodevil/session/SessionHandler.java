package com.bobodevil.session;

import io.vertx.rxcore.java.eventbus.RxMessage;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import rx.Observable;
import rx.util.functions.Action1;

public class SessionHandler {
	private Vertx vertx;
	private Container container;
	private JsonObject sessionConfig;
	
	public SessionHandler(Vertx vertx, Container container, JsonObject sessionConfig) {
		this.vertx = vertx;
		this.container = container;
		this.sessionConfig = sessionConfig;
		
		EventBus eb = vertx.eventBus();
		eb.registerHandler("com.bobodevil.session.id", getSessionId());
		eb.registerHandler("com.bobodevil.session.get", getObjectFromSession());
		eb.registerHandler("com.bobodevil.session.put", putObjectToSession());
		eb.registerHandler("com.bobodevil.session.delete", deleteObjectFromSession());
	}
	public Handler<Message<JsonObject>> getSessionId() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				String sessionId = message.body().getString("sessionId", "");
				final JsonObject jsonObject = new JsonObject();
				jsonObject.putString("status", "ok");
				if ("".equals(sessionId)) {
					
					vertx.eventBus().send(sessionConfig.getString("address"), new JsonObject().putString("action", "start"),
							new Handler<Message<JsonObject>>() {
								@Override
								public void handle(Message<JsonObject> sessionMessage) {
									container.logger().info	("id " + sessionMessage.body().getString("sessionId"));
									
									String sessionId = sessionMessage.body().getString("sessionId");
									jsonObject.putString("sessionId", sessionId);
									message.reply(jsonObject);
								}
							});
				} else {
					jsonObject.putString("sessionId", sessionId);
					message.reply(jsonObject);
				}
			}
		};
	}
	
	
	public Handler<Message<JsonObject>> getObjectFromSession() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				
				SessionFunction sessionFunction = new SessionFunction(vertx, container);
				container.logger().info("Working");
				
				
				Observable<RxMessage<JsonObject>> obs = sessionFunction.getRxEventBus().send("com.bobodevil.session.id", 
						new JsonObject().putString("sessionId", message.body().getString("sessionId","")));
				
				obs.subscribe(
					new Action1<RxMessage<JsonObject>>() {
						public void call(final RxMessage<JsonObject> rxMessage) {
							final String sessionId = rxMessage.body().getString("sessionId");
							JsonArray sessionType = message.body().getArray("sessionType");
							
							
							final JsonObject json = new JsonObject().putString("action", "get")
									.putString("sessionId", sessionId).putArray("fields", sessionType);
				        	
							final Integer eventTimeout = 20; //20ms;
							//Nullpoint if nothing in session
							vertx.eventBus().sendWithTimeout(
									sessionConfig.getString("address"), json, eventTimeout ,new Handler<AsyncResult<Message<JsonObject>>>() {
								@Override
								public void handle(AsyncResult<Message<JsonObject>> asyncMessage) {
									if (asyncMessage.succeeded()) {
										if (asyncMessage.result().body().getField("error") == null) {
											final JsonObject jsonObject = new JsonObject();
								        	jsonObject.putString("status", "ok");
								        	jsonObject.putString("dataType", "json");
								        	jsonObject.putString("sessionId", sessionId);
								        	container.logger().info("123");
								        	JsonObject dataObject = asyncMessage.result().body().getObject("data");
								        	
								        	if (dataObject != null) {
								        		String data = asyncMessage.result().body().getObject("data").toString();
								        		jsonObject.putString("data", data);
								        	} else {
								        		jsonObject.putString("data", "{}");
								        	}
								        	
								            container.logger().info("data " + jsonObject);
								        	message.reply(jsonObject);
										} else {
											final JsonObject jsonObject = new JsonObject();
								        	jsonObject.putString("status", "error");
								        	container.logger().info("data " + jsonObject);
								        	message.reply(jsonObject);
										}
									} else {
										final JsonObject jsonObject = new JsonObject();
							        	jsonObject.putString("status", "error");
							        	container.logger().info("data " + jsonObject);
							        	message.reply(jsonObject);
									}
								};
							});
							
						}
					},
					new Action1<Throwable>() {
						public void call(Throwable err) {
							container.logger().info("NOOO!!!" + err);
						}
					}
				);
	        	
			}
		};
	}
	
	
	public Handler<Message<JsonObject>> putObjectToSession() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				
				
	        	JsonObject jsonObject = new JsonObject();
	        	jsonObject.putString("status", "ok");
	        	jsonObject.putString("dataType", "json");
	        	jsonObject.putString("data", "addObjectToSession is working");
	            
	        	message.reply(jsonObject);
			}
		};
	}
	
	public Handler<Message<JsonObject>> deleteObjectFromSession() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
	        	JsonObject jsonObject = new JsonObject();
	        	jsonObject.putString("status", "ok");
	        	jsonObject.putString("data", "removeObjectFromSession is working");
	            message.reply(jsonObject);
			}
		};
	}
}
