package com.bobodevil.session;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import rx.util.functions.Action1;

public class SessionFunction {
	private Vertx vertx;
	private Container container;

	private RxEventBus rxEventBus;
	
	public SessionFunction(Vertx vertx, Container container) {
		this.vertx = vertx;
		this.container = container;
		this.rxEventBus = new RxEventBus(vertx.eventBus());

	}
	
	public RxEventBus getRxEventBus() {
		return rxEventBus;
	}
	
	
	
  
	
}
