package com.bobodevil;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import com.bobodevil.session.SessionHandler;


public class SessionVerticle extends Verticle {
	public void start() {
		JsonObject appConfig = container.config();
		
		JsonObject sessionManager = appConfig.getObject("session-manager");
		
		container.deployModule("com.campudus~session-manager~2.0.1-final"
				, sessionManager);
		
		SessionHandler sessionHandler = new SessionHandler(vertx ,container, sessionManager);
		
		
	}
}
