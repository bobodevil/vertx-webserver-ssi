package com.bobodevil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class ContainerVerticle extends Verticle {
	public void start() {
		JsonObject appConfig = container.config();
		
		if (appConfig.getObject("web-module", null) == null) {
			try {
				byte[] encoded = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("config.json").toURI()));
				container.logger().info(new String(encoded));
				appConfig = new JsonObject(new String(encoded));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	  
	  container.deployModule("com.bobodevil~template-module~0.1", appConfig.getObject("template-module"));
	  container.deployModule("com.bobodevil~logger-module~0.1", appConfig.getObject("logger-module"));
	  container.deployModule("com.bobodevil~server-side-include-module~0.1");
	  
	  container.deployModule("com.bobodevil~shoppingcart-module~0.1", appConfig.getObject("shopping-cart"));
	  
	  //container.deployModule("com.bobodevil~shopping-cart-js-module~0.1", appConfig.getObject("shopping-cart"));
	  
	  
	  container.deployModule("com.bobodevil~web-module~0.1", appConfig.getObject("web-module"));
	  container.deployModule("com.bobodevil~cms-module~0.1", appConfig.getObject("cms-module"));
	  container.deployModule("com.bobodevil~proxy-module~0.1", appConfig.getObject("proxy-module"));
	  container.deployModule("com.bobodevil~session-module~0.1", appConfig.getObject("session-module"));
	  container.deployModule("io.vertx~mod-mongo-persistor~2.1.0", appConfig.getObject("mongo-persistor"));
	  
	  container.logger().info("Container started");

  }
}