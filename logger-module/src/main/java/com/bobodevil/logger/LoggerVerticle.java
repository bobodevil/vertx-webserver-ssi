package com.bobodevil.logger;


import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.json.JsonObject;



public class LoggerVerticle extends LoggerHandler {

	public void start() {
		
		JsonObject appConfig = container.config();
		
		EventBus eb = vertx.eventBus();
		//appConfig.getObject("log-location")
		
		eb.registerHandler("com.bobodevil.logger.debug", debugLoggingHandler());
		eb.registerHandler("com.bobodevil.logger.info", infoLoggingHandler());
		
		
		container.logger().info("Logger Handler started");
	}
}
