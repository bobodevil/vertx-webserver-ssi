package com.bobodevil.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUtils {
	private Logger log;
	
	public LoggerUtils(String logName) {
		log = LoggerFactory.getLogger(logName);
	}
	
	public void debug(String arg0, Object arg1) {
		log.debug(arg0, arg1);
	}
	
	public void warn(String arg0, Object arg1) {
		log.warn(arg0, arg1);
	}
	
	public void info(String arg0, Object arg1) {
		log.info(arg0, arg1);
	}
}
