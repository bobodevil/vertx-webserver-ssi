package com.bobodevil.logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.CaseInsensitiveMultiMap;
import org.vertx.java.core.http.HttpClient;
import org.vertx.java.core.http.HttpClientRequest;
import org.vertx.java.core.http.HttpClientResponse;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;
import org.vertx.java.platform.Verticle;


public class LoggerHandler extends Verticle {
	public Handler<Message<JsonObject>> debugLoggingHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				container.logger().debug(message.body().getString("data"));
				message.reply();
			}
		};
	}
	public Handler<Message<JsonObject>> infoLoggingHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				container.logger().info(message.body().getString("data"));
				message.reply();
			}
		};
	}
}
