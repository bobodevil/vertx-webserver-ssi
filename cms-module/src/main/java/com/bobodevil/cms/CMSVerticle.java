package com.bobodevil.cms;

import org.vertx.java.busmods.BusModBase;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;


public class CMSVerticle extends BusModBase {
	
	
	public void start() {
		super.start();
		EventBus eb = vertx.eventBus();
		
		eb.registerHandler("com.bobodevil.cms.request", cmsRequestHandler());
		
		
		container.logger().info("CMS Verticle started");
	}
	
	public Handler<Message<JsonObject>> cmsRequestHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				
				JsonObject jsonObject = new JsonObject();
				jsonObject.putString("method", message.body().getString("method"));
				jsonObject.putString("uri", message.body().getString("uri"));
				jsonObject.putObject("headers", message.body().getObject("headers"));
				jsonObject.putString("sessionId", message.body().getString("sessionId"));
				
				eb.send("com.bobodevil.proxy.cms", jsonObject, 
						new Handler<Message<JsonObject>>() {
							public void handle(Message<JsonObject> cmsMessage) {
					            final JsonObject messageBody = cmsMessage.body();
					            String data = messageBody.getString("data");
					            
					            //TODO : May consider caching this
					            
					            JsonObject response = new JsonObject();
					            response.putString("status", "ok");
					            response.putString("data", data);
					            response.putObject("responseHeader", messageBody.getObject("responseHeader"));
					            
					            message.reply(response);
							}
						}
				);
				
			}
		};
	}
}
 