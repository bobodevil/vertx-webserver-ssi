var vertx = require('vertx')
var console = require('vertx/console')

var eb = vertx.eventBus;



var displayShoppingCartHandler = function(message, replier) {
	console.log('displayShoppingCartHandler start');
	var shoppingCartHandlerMessage = message;
	var shoppingCartHandlerReplier = replier;
	
	var jsonObject = {
		template: shoppingCartHandlerMessage.moduleTemplate,
		data: 'Shopping Cart'
	}
	eb.send("com.bobodevil.template", jsonObject, function(message, replier) {
		
		var responseObject = {
			status : "ok",
			data : message.data
		}
		console.log('js response object' + responseObject);
		shoppingCartHandlerReplier(responseObject);
	});
}


eb.registerHandler('com.bobodevil.module.shoppingcart.display', displayShoppingCartHandler);


console.log('Shopping Cart JS Handler Started');