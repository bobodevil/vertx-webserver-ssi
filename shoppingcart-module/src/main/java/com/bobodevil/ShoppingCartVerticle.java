package com.bobodevil;


import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import com.bobodevil.shoppingcart.ShoppingCartHandler;


public class ShoppingCartVerticle extends Verticle {

	public void start() {
		super.start();
		EventBus eb = vertx.eventBus();
		
		ShoppingCartHandler scHandler = new ShoppingCartHandler(vertx, container);
		
		
        
		container.logger().info("shoppingcartAPIHander started");
	}
}
