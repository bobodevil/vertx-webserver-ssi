package com.bobodevil.shoppingcart.model;

import java.util.List;

public class ShoppingCartModel {
	
	private List<ProductModel> products;

	public List<ProductModel> getProducts() {
		return products;
	}

	public void setProducts(List<ProductModel> products) {
		this.products = products;
	}
	
}
