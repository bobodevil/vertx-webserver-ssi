package com.bobodevil.shoppingcart;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import rx.Observable;
import rx.util.functions.Action1;
import rx.util.functions.Func1;

public class ShoppingCartFunction {
	private Vertx vertx;
	private Container container;
	private RxEventBus rxEventBus;
	
	public ShoppingCartFunction(Vertx vertx, Container container) {
		this.vertx = vertx;
		this.container = container;
		this.rxEventBus = new RxEventBus(vertx.eventBus());

	}
	
	public RxEventBus getRxEventBus() {
		return rxEventBus;
	}
	
	public Func1<RxMessage<JsonObject>, Observable<RxMessage<JsonObject>>> getShoppingCart() {
		return new Func1<RxMessage<JsonObject>, Observable<RxMessage<JsonObject>>>() {
			@Override
			public Observable<RxMessage<JsonObject>> call(RxMessage<JsonObject> jsonObjectRxMessage) {
				JsonObject requestShoppingCart = new JsonObject();
				requestShoppingCart.putString("sessionId", jsonObjectRxMessage.body().getString("sessionId"));
				
				return rxEventBus.send("com.bobodevil.session.getId", requestShoppingCart);
			}
		};
	}
	
	public Action1<RxMessage<JsonObject>> getRenderedShoppingCart(final JsonObject jsonObject, final Message<JsonObject> message) {
		return new Action1<RxMessage<JsonObject>>() {
			public void call(RxMessage<JsonObject> rxMessage) {
				jsonObject.putString("sessionId", rxMessage.body().getString("sessionId", ""));	
				jsonObject.putString("data", rxMessage.body().getString("data", "{}"));
				vertx.eventBus().send("com.bobodevil.template", jsonObject, new Handler<Message<JsonObject>>() {
					public void handle(Message<JsonObject> templateMessage) {
						JsonObject responseObject = new JsonObject();
						responseObject.putString("status", "ok");
						responseObject.putString("sessionId", templateMessage.body().getString("sessionId", ""));	
						String data = templateMessage.body().getString("data");
						responseObject.putString("data", data);
						message.reply(responseObject);
					}
				});
			}
		};
    }
	
}
