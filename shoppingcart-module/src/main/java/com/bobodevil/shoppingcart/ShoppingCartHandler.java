package com.bobodevil.shoppingcart;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;
import org.vertx.java.platform.Verticle;

import rx.Observable;
import rx.util.functions.Action1;
import rx.util.functions.Func0;
import rx.util.functions.Func1;


public class ShoppingCartHandler  {
	private Vertx vertx;
	private Container container;
	
	public ShoppingCartHandler(Vertx vertx, Container container) {
		this.vertx = vertx;
		this.container = container;
		vertx.eventBus().registerHandler("com.bobodevil.module.shoppingcart.display", displayHandler());
		vertx.eventBus().registerHandler("com.bobodevil.module.shoppingcart.dispatchhandler", dispatchHandler());
		vertx.eventBus().registerHandler("com.bobodevil.module.shoppingcart.get", getHandler());
		vertx.eventBus().registerHandler("com.bobodevil.module.shoppingcart.delete", deleteHandler());
		vertx.eventBus().registerHandler("com.bobodevil.module.shoppingcart.put", putHandler());
	}
	
	public Handler<Message<JsonObject>> dispatchHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				JsonObject incomingObject = message.body();
				final String method = incomingObject.getString("method").toLowerCase();
				final Integer eventTimeout = 1000; //100ms;
				final String moduleEvent = "com.bobodevil.module.shoppingcart."+ method;
				vertx.eventBus().sendWithTimeout(
						moduleEvent, incomingObject, eventTimeout ,new Handler<AsyncResult<Message<JsonObject>>>() {
						@Override
						public void handle(AsyncResult<Message<JsonObject>> asyncMessage) {
							JsonObject responseObject = new JsonObject();
							if (asyncMessage.succeeded()) {
								Message<JsonObject> eventmessage = asyncMessage.result();
					        	responseObject.putString("status", "ok");
					        	responseObject.putString("data", eventmessage.body().getString("data"));
							}
							else {
								responseObject.putString("status", "error");
								responseObject.putString("message", asyncMessage.cause().getMessage());
								responseObject.putString("data", "");
							}
							message.reply(responseObject);
						}
					}
				);
			}
		};
	}
	
	public Handler<Message<JsonObject>> displayHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				container.logger().info("retrieve event " + "com.bobodevil.shoppingcart");
				final JsonObject jsonObject = new JsonObject();
				jsonObject.putString("template", message.body().getString("moduleTemplate",""));
				ShoppingCartFunction scFunction = new ShoppingCartFunction(vertx, container);
				
				
				Observable<RxMessage<JsonObject>> obs = scFunction.getRxEventBus().send("com.bobodevil.session.get", 
											new JsonObject()
												.putString("sessionId", message.body().getString("sessionId"))
												.putArray("sessionType", new JsonArray().addString("shoppingcart"))
														);
                
				Action1<RxMessage<JsonObject>> renderingAction = scFunction.getRenderedShoppingCart(jsonObject, message);
				
				//Could be a json
				
				obs.subscribe(renderingAction,
				  new Action1<Throwable>() {
				    public void call(Throwable err) {
				    	container.logger().info("FAiled!!!");
				    	message.reply();
				    }
				  }
				);
				
				
			}
		};
	}
	
	public Handler<Message<JsonObject>> getHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				container.logger().info("getHandler event " + "com.bobodevil.shoppingcart");
	        	JsonObject jsonObject = new JsonObject();
	        	jsonObject.putString("status", "ok");
	        	jsonObject.putString("data", "Shoppingcart getHandler is working");
	            message.reply(jsonObject);
	            
	           // vertx.eventBus().send("com.bobodevil.session.getId", json, new ReplyHandler(req, data, genericObject));
	            
			}
		};
	}
	
	public Handler<Message<JsonObject>>  deleteHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				container.logger().info("deleteHandler event " + "com.bobodevil.shoppingcart");
	        	JsonObject jsonObject = new JsonObject();
	        	jsonObject.putString("status", "ok");
	        	jsonObject.putString("data", "Shoppingcart deleteHandler is working");
	            message.reply(jsonObject);
	            
	           // vertx.eventBus().send("com.bobodevil.session.getId", json, new ReplyHandler(req, data, genericObject));
	            
			}
		};
	}
	
	
	public Handler<Message<JsonObject>> putHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				JsonObject incomingObject = message.body().getObject("params");
				
	        	JsonObject jsonObject = new JsonObject();
	        	jsonObject.putString("status", "ok");
	        	jsonObject.putString("data", "Shoppingcart putHandler is working");
	            message.reply(jsonObject);
			}
		};
	}


}
