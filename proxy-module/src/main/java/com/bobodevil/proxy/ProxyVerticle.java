package com.bobodevil.proxy;


import org.vertx.java.busmods.BusModBase;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;



public class ProxyVerticle extends BusModBase {

	public void start() {
		super.start();
		
		JsonObject appConfig = container.config();
		
		EventBus eb = vertx.eventBus();
		
		ProxyServerHandler cmsProxy = new ProxyServerHandler(vertx , appConfig.getObject("cms-proxy-setting"), container);
		
		eb.registerHandler("com.bobodevil.proxy.cms", cmsProxy.handleRequest());
		container.logger().info("proxy Handler started");
	}
}
