package com.bobodevil.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.MultiMap;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.CaseInsensitiveMultiMap;
import org.vertx.java.core.http.HttpClient;
import org.vertx.java.core.http.HttpClientRequest;
import org.vertx.java.core.http.HttpClientResponse;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;


public class ProxyServerHandler{
	
	private Vertx vertx;
	private Container container;
	final HttpClient client;

	public ProxyServerHandler(Vertx vertx, JsonObject json, Container container) {
		this.vertx = vertx;
		this.container = container;
		
		client = this.vertx.createHttpClient()
					.setHost(json.getString("address"))
						.setPort(json.getInteger("port")).setMaxPoolSize(json.getInteger("maxPoolSize"));
	}

	public Handler<Message<JsonObject>> handleRequest() {
		return new Handler<Message<JsonObject>>() {
			public void handle(final Message<JsonObject> message) {
				
				final JsonObject jsonObject = message.body();
				final Map<String,Object> headerMap = jsonObject.getObject("headers").toMap();
				MultiMap headerMultiMap = new CaseInsensitiveMultiMap();
				for (String key : headerMap.keySet()) {
					headerMultiMap.add(key, (String)headerMap.get(key));
				}
				
				final HttpClientRequest proxyReq = getClientRequest(
							jsonObject.getString("method"), 
							jsonObject.getString("uri"),
							message);
				
				proxyReq.headers().add(headerMultiMap);
				proxyReq.setChunked(true);
				//proxyReq.write(data);
				proxyReq.end();
			}
			
		};
		
	    
	}

	
	private HttpClientRequest getClientRequest(final String method, 
							final String uri, final Message<JsonObject> message) {
		final JsonObject returnObject = new JsonObject();
		
		//container.logger().info("Proxying request: " + uri);
		
		final HttpClientRequest proxyReq = client.request(method, uri, new Handler<HttpClientResponse>(){
			public void handle(HttpClientResponse serverRes) {
				//container.logger().info("Proxying response: " + serverRes.statusCode());
				returnObject.putNumber("statusCode", serverRes.statusCode());
				JsonObject responseheaders = new JsonObject();
				for (java.util.Map.Entry<String,String> entry: serverRes.headers().entries()) {
					responseheaders.putString(entry.getKey(), entry.getValue());
				}
				returnObject.putObject("responseHeader", responseheaders);
				
				final StringBuffer data = new StringBuffer("");
				
				serverRes.dataHandler(new Handler<Buffer>(){
					public void handle(Buffer bufferData) {
						//container.logger().info("Proxying response body: " + bufferData);
						data.append(bufferData);
					}
				});
				serverRes.endHandler(new Handler<Void>(){
					@Override
					public void handle(Void v) {
						returnObject.putString("data", data.toString());
						message.reply(returnObject);
					}
				});
			}
		});
		return proxyReq;
		
	}
}
